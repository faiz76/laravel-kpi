<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_kpi_detail', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('header_id')->nullable();
            $table->foreign('header_id')->references('id')->on('tr_kpi_header');
            $table->unsignedInteger('item_id')->nullable();
            $table->foreign('item_id')->references('id')->on('ms_kpi_item_category');
            $table->decimal('target', 17, 2)->nullable();
            $table->decimal('actual', 17, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_kpi_detail');
    }
};
