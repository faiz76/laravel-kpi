<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_kpi_header', function (Blueprint $table) {
            $table->id();
            $table->string('site_code');
            $table->foreign('site_code')->references('siteID')->on('ms_site');
            $table->date('data_date');
            $table->timestamps();
            $table->string('created_by', 16)->nullable();
            $table->string('updated_by', 16)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_kpi_header');
    }
};
