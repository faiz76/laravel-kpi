<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_kpi_category', function (Blueprint $table) {
            $table->id();
            $table->string('category_item', 60)->nullable(false);
            $table->unsignedInteger('perspective_id');
            $table->foreign('perspective_id')->references('id')->on('ms_kpi_perspective');
            $table->string('created_by', 16)->nullable();
            $table->string('updated_by', 16)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_kpi_category');
    }
};
