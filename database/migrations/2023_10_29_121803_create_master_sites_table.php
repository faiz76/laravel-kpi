<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_site', function (Blueprint $table) {
            $table->string('siteID', 3)->primary();
            $table->string('companyID', 4);
            $table->string('siteName', 50)->nullable(false);
            $table->unsignedInteger('cluster_id');
            $table->foreign('cluster_id')->references('id')->on('ms_cluster')->cascadeOnDelete();
            $table->string('createdBy', 50)->nullable();
            $table->string('updatedBy', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_site');
    }
};
