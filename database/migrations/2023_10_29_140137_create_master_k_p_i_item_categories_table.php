<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_kpi_item_category', function (Blueprint $table) {
            $table->id();
            $table->string('item_name', 60);
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('ms_kpi_category');
            $table->string('created_by', 16)->nullable();
            $table->string('updated_by', 16)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('weight');
            $table->enum('mtd_option', ['MAX', 'MIN'])->nullable();
            $table->enum('ytd_option', ['MAX', 'MIN'])->nullable();
            $table->enum('is_active', ['YES', 'NO'])->default('YES');
            $table->enum('shown_in_focus', ['YES', 'NO'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_kpi_item_category');
    }
};
