<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_company_code', function (Blueprint $table) {
            $table->string('company_code', 10)->primary()->nullable(false);
            $table->string('company_name', 50)->nullable(false);
            $table->string('createdBy', 50)->nullable(false);
            $table->string('updatedBy', 50)->nullable(false);
            $table->timestamp('created_at')->nullable(true)->useCurrent();
            $table->timestamp('updated_at')->nullable(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_company_code');
    }
};
