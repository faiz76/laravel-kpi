<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ms_employee', function (Blueprint $table) {
            $table->string('EmployeeID')->primary();
            $table->foreign('EmployeeID')->on('users')->references('nik');
            $table->string('EmployeeName', 50);
            $table->string('EmployeeAddress', 150)->nullable();
            $table->integer('EmployeePhone')->nullable();
            $table->string('SiteId', 10);
            $table->integer('DeptID');
            $table->string('CreatedBy', 50);
            $table->string('UpdatedBy', 50);
            $table->timestamps();
            $table->string('EMAIL_ADDRESS', 60)->nullable();
            $table->string('EmploymentType', 30)->nullable();
            $table->string('Class', 5)->nullable();
            $table->string('Status', 8)->nullable();
            $table->string('PositionName', 60)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ms_employee');
    }
};
