<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $users = [
        //     [
        //         'id' => 90023011,
        //         'name' => 'Jonathan Ferdaninsyah Julyano',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90018003,
        //         'name' => 'Novi Nuraini Hasanah',
        //         'email' => 'novi.nh@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90018021,
        //         'name' => 'Lukman Nukhir',
        //         'email' => 'lukman.nukhir@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => '$2y$10$FKx6RYzxgD9SMAvLR4AnWeZeRJmkQ0P2QwKfguJMS4NvM1YBry.Ay'
        //     ],
        //     [
        //         'id' => 90019079,
        //         'name' => 'Firmansyah',
        //         'email' => 'firmansyah@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => '$2y$10$yONaIzHebp4hP4KzpZD01eFWdln/.G2pEsygBkk6d4mWpPp9Yoi62'
        //     ],
        //     [
        //         'id' => 90019081,
        //         'name' => 'Dhana Trisna Putra',
        //         'email' => 'dhana.trisna@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => '$2y$10$GhgjlxZaCxqr4EI8Nnwd0ePrmxdwINX5QZ7x5C95PWhrLNSufpd8y'
        //     ],
        //     [
        //         'id' => 90020001,
        //         'name' => 'Netanya Panggabean',
        //         'email' => 'netanya.panggabean@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => '$2y$10$7h4eGAWccNplKq0pF7lWk.CXJhaRngCcPwQaJRKK3iUJUR7BB9S2a'
        //     ],
        //     [
        //         'id' => 90020003,
        //         'name' => 'Adham Pamungkas Wardoyo',
        //         'email' => 'adham.wardoyo@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90021004,
        //         'name' => 'Ridho Jhulang Aqsha',
        //         'email' => 'ridho.aqsha@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90022001,
        //         'name' => 'Niken Dwi Nirma',
        //         'email' => 'niken.dwi@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90022057,
        //         'name' => 'Fareladzin Wibi Ramadhan',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90023007,
        //         'name' => 'Muhammad Aldi Putra',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90023008,
        //         'name' => 'Febriari Candra Guritno',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90018024,
        //         'name' => 'Usman Abdul Rahman',
        //         'email' => 'usman@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90122001,
        //         'name' => 'Adian Ilham Ramadhan',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90020004,
        //         'name' => 'Anak Agung Ngurah Agus',
        //         'email' => null,
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90012030,
        //         'name' => 'Taufiq',
        //         'email' => 'taufiq@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        //     [
        //         'id' => 90019003,
        //         'name' => 'Desi Arniati',
        //         'email' => 'desi.arniati@hpu-mining.com',
        //         'nik' => '317312341234' . rand(1000, 9999),
        //         'password' => Hash::make('admin123')
        //     ],
        // ];
        $users = [
            [
                'nik' => 11111111,
                'name' => 'Faiz',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90023011,
                'name' => 'Jonathan Ferdaninsyah Julyano',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90018003,
                'name' => 'Novi Nuraini Hasanah',
                'email' => 'novi.nh@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90018021,
                'name' => 'Lukman Nukhir',
                'email' => 'lukman.nukhir@hpu-mining.com',
                'password' => '$2y$10$FKx6RYzxgD9SMAvLR4AnWeZeRJmkQ0P2QwKfguJMS4NvM1YBry.Ay'
            ],
            [
                'nik' => 90019079,
                'name' => 'Firmansyah',
                'email' => 'firmansyah@hpu-mining.com',
                'password' => '$2y$10$yONaIzHebp4hP4KzpZD01eFWdln/.G2pEsygBkk6d4mWpPp9Yoi62'
            ],
            [
                'nik' => 90019081,
                'name' => 'Dhana Trisna Putra',
                'email' => 'dhana.trisna@hpu-mining.com',
                'password' => '$2y$10$GhgjlxZaCxqr4EI8Nnwd0ePrmxdwINX5QZ7x5C95PWhrLNSufpd8y'
            ],
            [
                'nik' => 90020001,
                'name' => 'Netanya Panggabean',
                'email' => 'netanya.panggabean@hpu-mining.com',
                'password' => '$2y$10$7h4eGAWccNplKq0pF7lWk.CXJhaRngCcPwQaJRKK3iUJUR7BB9S2a'
            ],
            [
                'nik' => 90020003,
                'name' => 'Adham Pamungkas Wardoyo',
                'email' => 'adham.wardoyo@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90021004,
                'name' => 'Ridho Jhulang Aqsha',
                'email' => 'ridho.aqsha@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90022001,
                'name' => 'Niken Dwi Nirma',
                'email' => 'niken.dwi@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90022057,
                'name' => 'Fareladzin Wibi Ramadhan',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90023007,
                'name' => 'Muhammad Aldi Putra',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90023008,
                'name' => 'Febriari Candra Guritno',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90018024,
                'name' => 'Usman Abdul Rahman',
                'email' => 'usman@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90122001,
                'name' => 'Adian Ilham Ramadhan',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90020004,
                'name' => 'Anak Agung Ngurah Agus',
                'email' => null,
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90012030,
                'name' => 'Taufiq',
                'email' => 'taufiq@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
            [
                'nik' => 90019003,
                'name' => 'Desi Arniati',
                'email' => 'desi.arniati@hpu-mining.com',
                'password' => Hash::make('admin123')
            ],
        ];
        foreach ($users as $user) {
            User::create(
                $user
            );
        }
    }
}
