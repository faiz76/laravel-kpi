<?php

namespace Database\Seeders;

use App\Models\MasterKPICategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterKPICategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'category_item' => 'Provitability',
                'perspective_id' => 1,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:22:54.734',
                'updated_at' => null,
            ],
            [
                'category_item' => 'Cashflow',
                'perspective_id' => 1,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:23:13.742',
                'updated_at' => null,
            ],
            [
                'category_item' => 'Strategic Partnership',
                'perspective_id' => 2,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:23:57.699',
                'updated_at' => null,
            ],
            [
                'category_item' => 'HSE Generative Culture',
                'perspective_id' => 3,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:24:23.687',
                'updated_at' => null,
            ],
            [
                'category_item' => 'Maximized Asset Productivity',
                'perspective_id' => 3,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:24:58.188',
                'updated_at' => null,
            ],
            [
                'category_item' => 'Maximized Asset Utilization',
                'perspective_id' => 3,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:25:40.561',
                'updated_at' => null,
            ],
            [
                'category_item' => 'Develop & Retain Key Person',
                'perspective_id' => 4,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:26:03.697',
                'updated_at' => null,
            ],
        ];

        foreach ($categories as $category) {
            MasterKPICategory::create($category);
        }
    }
}
