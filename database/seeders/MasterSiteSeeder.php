<?php

namespace Database\Seeders;

use App\Models\MasterSite;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sites = [
            [
                "companyID" => "HPMU",
                "siteID" => "KDA",
                "siteName" => "Kendawangan",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "DMI",
                "siteName" => "Damai",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "DTA",
                "siteName" => "Delta",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "KJA",
                "siteName" => "Kaltim Jaya Bara",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "KUP",
                "siteName" => "Karya Usaha Pertiwi",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "MGA",
                "siteName" => "Marunda Graha Mineral",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "HPA",
                "siteName" => "Hamparan Anugrah Abadi",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "KMO",
                "siteName" => "Klubir Mining Operation",
                "cluster_id" => 1,
                "createdBy" => "Dhana Trisna Putra",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPMU",
                "siteID" => "MBA",
                "siteName" => "Marina Bara Lestari",
                "cluster_id" => 1,
                "createdBy" => "Dhana Trisna Putra",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPMU",
                "siteID" => "OBI",
                "siteName" => "HARITA NIKEL OBI",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "SAB",
                "siteName" => "Semesta Alam Barito",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "JKA",
                "siteName" => "Head Office Jakarta (HPU)",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPMU",
                "siteID" => "JTA",
                "siteName" => "Head Office Jakarta (HPMU)",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "BDA",
                "siteName" => "Batubara Duaribu Abadi",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "BMI",
                "siteName" => "Bumi Barito Mineral",
                "cluster_id" => 1,
                "createdBy" => "Dhana Trisna Putra",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "GMA",
                "siteName" => "Ganda Alam Makmur",
                "cluster_id" => 1,
                "createdBy" => "Dhana Trisna Putra",
                "updatedBy" => "Dhana Trisna Putra"
            ],
            [
                "companyID" => "HPMU",
                "siteID" => "AMO",
                "siteName" => "Malinau",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "PPA",
                "siteName" => "Putra Perkasa Abadi",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPU",
                "siteID" => "HRC",
                "siteName" => "HPU Rebuild Center",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ],
            [
                "companyID" => "HPMU",
                "siteID" => "SCA",
                "siteName" => "Sulawesi Cahaya Mineral",
                "cluster_id" => 1,
                "createdBy" => "smsSystem",
                "updatedBy" => "smsSystem"
            ]
        ];
        foreach ($sites as $site) {
            MasterSite::create(
                $site
            );
        }
    }
}
