<?php

namespace Database\Seeders;

use App\Models\MasterKPIPerspective;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterKPIPerspectiveSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perspectives = [
            [
                'perspective_item' => 'Financial Perspective',
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:16:29.238',
                'updated_at' => '2023-01-24 01:16:29.243',
            ],
            [
                'perspective_item' => 'Customer Perspective',
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:16:44.442',
                'updated_at' => '2023-01-24 01:16:44.442',
            ],
            [
                'perspective_item' => 'Internal Process Perspective',
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:17:08.442',
                'updated_at' => '2023-01-24 01:17:08.442',
            ],
            [
                'perspective_item' => 'Learning & Growth Perspective',
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:17:37.348',
                'updated_at' => '2023-01-24 01:17:37.348',
            ],
        ];
        foreach ($perspectives as $perspective) {
            MasterKPIPerspective::create(
                $perspective
            );
        }
    }
}
