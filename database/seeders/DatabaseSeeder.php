<?php

namespace Database\Seeders;

use App\Models\MasterCompanyCode;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            EmployeeSeeder::class,
            MasterKPIPerspectiveSeeder::class,
            MasterKPICategorySeeder::class,
            MasterKPIItemCategorySeeder::class,
            MasterCompanyCodeSeeder::class,
            MasterClusterSeeder::class,
            MasterSiteSeeder::class
        ]);
    }
}
