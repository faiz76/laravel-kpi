<?php

namespace Database\Seeders;

use App\Models\MasterCluster;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clusters = [
            [
                'cluster_code' => 'HPU',
                'cluster_description' => 'Harmoni Panca Utama'
            ]
        ];
        foreach ($clusters as $cluster) {
            MasterCluster::create(
                $cluster
            );
        }
    }
}
