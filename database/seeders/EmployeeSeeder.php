<?php

namespace Database\Seeders;

use App\Models\Employee;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    public function run()
    {
        $employees = [
            [
                'EmployeeID' => '11111111',
                'EmployeeName' => 'Faiz',
                'EmployeeAddress' => 'Jl xxx',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2023-07-05 03:01:45.000',
                'updated_at' => '2023-07-12 03:01:05.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => 'Probation',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Admin Maybe ?',
            ],
            [
                'EmployeeID' => '90023011',
                'EmployeeName' => 'Jonathan Ferdaninsyah Julyano',
                'EmployeeAddress' => 'Jl. Pulo Nangka No. 43 RT 009/002 Kel. Rawa Buaya Kec. Cengkareng Jakarta Barat DKI Jakarta',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2023-07-05 03:01:45.000',
                'updated_at' => '2023-07-12 03:01:05.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => 'Probation',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Helpdesk Foreman',
            ],
            [
                'EmployeeID' => '90018003',
                'EmployeeName' => 'Novi Nuraini Hasanah',
                'EmployeeAddress' => 'Jl. Akasia 4 No. 148 RT.007 RW.003 Sukmajaya, Mekarjaya, Depok',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'novi.nh@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Helpdesk Foreman',
            ],
            [
                'EmployeeID' => '90018021',
                'EmployeeName' => 'Lukman Nukhir',
                'EmployeeAddress' => 'Jl. Pahlawan Revolusi No.2 RT.001 RW.002, Kel. Pondok Bambu, Kec. Duren Sawit, Jakarta Timur',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'lukman.nukhir@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'V',
                'Status' => 'AKTIF',
                'PositionName' => 'Infrastructure & Support Services Superintendent',
            ],
            [
                'EmployeeID' => '90019079',
                'EmployeeName' => 'Firmansyah',
                'EmployeeAddress' => 'Jl Veteran Raya Rt 001/03 No 24 Marga Java Kota Bekasi',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'firmansyah@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'IV',
                'Status' => 'AKTIF',
                'PositionName' => 'Infrastructure Services Supervisor',
            ],
            [
                'EmployeeID' => '90019081',
                'EmployeeName' => 'Dhana Trisna Putra',
                'EmployeeAddress' => 'KP Dukuh RT/RW 001/007 Desa Sudimara Selatan Kec Ciledug',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'dhana.trisna@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'IV',
                'Status' => 'AKTIF',
                'PositionName' => 'Software Engineer Supervisor',
            ],
            [
                'EmployeeID' => '90020001',
                'EmployeeName' => 'Netanya Panggabean',
                'EmployeeAddress' => 'Komplek Tasbi II Blok II No 45 LK IX',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'netanya.panggabean@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'IV',
                'Status' => 'AKTIF',
                'PositionName' => 'SAP Internal Consultant Supervisor',
            ],
            [
                'EmployeeID' => '90020003',
                'EmployeeName' => 'Adham Pamungkas Wardoyo',
                'EmployeeAddress' => null,
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'adham.wardoyo@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'IV',
                'Status' => 'AKTIF',
                'PositionName' => 'Software Engineer Supervisor',
            ],
            [
                'EmployeeID' => '90021004',
                'EmployeeName' => 'Ridho Jhulang Aqsha',
                'EmployeeAddress' => 'Jl Dr Wahidin Sh 1 No 2',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'ridho.aqsha@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90022001',
                'EmployeeName' => 'Niken Dwi Nirma',
                'EmployeeAddress' => 'Dusun Sidasari Kulon RT/RW 001/001 Kel/Desa Kubangkangkung Kecamatan Kawunganten Kabupaten Cilacap Provinsi Jawa Tengah',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'niken.dwi@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90022057',
                'EmployeeName' => 'Fareladzin Wibi Ramadhan',
                'EmployeeAddress' => 'Jl. Keben II a/4 RT/RW 005/011 Kel/Desa Bandungrejosari Kecamatan Sukun Kota Malang',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => 'Permanent',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90023007',
                'EmployeeName' => 'Muhammad Aldi Putra',
                'EmployeeAddress' => 'Kp. Cikumpa Gg. Triti 2 RT 003/010 Kel. Sukmajaya Kec. Sukmajaya Kota Depok Jawa Barat',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2023-05-03 15:32:44.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => 'Probation',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. SAP Internal Consultant',
            ],
            [
                'EmployeeID' => '90023008',
                'EmployeeName' => 'Febriari Candra Guritno',
                'EmployeeAddress' => 'Jl. Merak RT 008 Kel. Sidorejo Kec. Arut Selatan Kab. Kotawaringin Barat Kalimantan Tengah',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2023-05-03 15:32:44.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => 'Probation',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90018024',
                'EmployeeName' => 'Usman Abdul Rahman',
                'EmployeeAddress' => 'Cisalam Kubang No.-, RT.005 RW.004, Kel. Karikil, Kec. Mangkubumi, Kota Tasikmalaya',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2023-05-03 15:32:44.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'usman@hpu-mining.com',
                'EmploymentType' => 'IV',
                'Class' => 'III',
                'Status' => 'AKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90122001',
                'EmployeeName' => 'Adian Ilham Ramadhan',
                'EmployeeAddress' => 'Jl. Empulana no. 119 RT/RW 001/003 Kel/Desa Balongsari Kecamatan Magersari Kota Mojokerto Provinsi Jawa Timur',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => null,
                'Class' => 'IV',
                'Status' => 'NONAKTIF',
                'PositionName' => 'Jr. Software Engineer',
            ],
            [
                'EmployeeID' => '90020004',
                'EmployeeName' => 'Anak Agung Ngurah Agus',
                'EmployeeAddress' => null,
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2022-11-22 03:01:28.000',
                'EMAIL_ADDRESS' => null,
                'EmploymentType' => null,
                'Class' => 'IV',
                'Status' => 'NONAKTIF',
                'PositionName' => 'Software Engineer Supervisor',
            ],
            [
                'EmployeeID' => '90012030',
                'EmployeeName' => 'Taufiq',
                'EmployeeAddress' => 'Jl. Elektron III Blok AA - 16 RT.003 RW.012 Kel. Bringin, Kec. Ngaliyan, Semarang',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:19.000',
                'EMAIL_ADDRESS' => 'taufiq@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'VI',
                'Status' => 'AKTIF',
                'PositionName' => 'CMIS Manager',
            ],
            [
                'EmployeeID' => '90019003',
                'EmployeeName' => 'Desi Arniati',
                'EmployeeAddress' => 'Jl. Penggalang III No. 22 RT. 29 Kel. Damai Kec. Balikpapan Kota Kalimantan Timur',
                'EmployeePhone' => null,
                'SiteId' => 'JKA',
                'DeptID' => 18,
                'CreatedBy' => 'UPDATE_BY_SYSTEM',
                'UpdatedBy' => 'UPDATE_BY_SYSTEM',
                'created_at' => '2022-09-30 00:00:00.000',
                'updated_at' => '2023-05-29 08:38:13.000',
                'EMAIL_ADDRESS' => 'desi.arniati@hpu-mining.com',
                'EmploymentType' => 'Permanent',
                'Class' => 'IV',
                'Status' => 'AKTIF',
                'PositionName' => 'SAP ABAPER Supervisor',
            ]
        ];
        foreach ($employees as $employee) {
            Employee::create(
                $employee
            );
        }
    }
}
