<?php

namespace Database\Seeders;

use App\Models\MasterCompanyCode;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterCompanyCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company_codes = [
            [
                'company_code' => 'HPMU',
                'company_name' => 'Hasta Panca Mandiri Utama',
                'createdBy' => 1234123412341234,
                'updatedBy' => 1234123412341234,
            ],
            [
                'company_code' => 'HPU',
                'company_name' => 'Harmoni Panca Utama',
                'createdBy' => 1234123412341234,
                'updatedBy' => 1234123412341234,
            ]
        ];
        foreach ($company_codes as $company_code) {
            MasterCompanyCode::create(
                $company_code
            );
        }
    }
}
