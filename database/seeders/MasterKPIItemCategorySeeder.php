<?php

namespace Database\Seeders;

use App\Models\MasterKPIItemCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterKPIItemCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
                'item_name' => 'Revenue',
                'category_id' => 1,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:42:34.259',
                'updated_at' => null,
                'weight' => 5,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'COGS',
                'category_id' => 1,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:42:49.4',
                'updated_at' => null,
                'weight' => 0,
                'mtd_option' => 'MIN',
                'ytd_option' => 'MIN',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'Collectability',
                'category_id' => 2,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:43:03.446',
                'updated_at' => null,
                'weight' => 5,
                'mtd_option' => 'MIN',
                'ytd_option' => 'MIN',
                'is_active' => 'YES',
                'shown_in_focus' => 'NO',
            ],
            [
                'item_name' => 'Contract on Hand',
                'category_id' => 3,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:43:22.287',
                'updated_at' => null,
                'weight' => 5,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'HSE Index',
                'category_id' => 4,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:44:15.591',
                'updated_at' => null,
                'weight' => 0,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'Productivity Asset',
                'category_id' => 5,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:44:30.369',
                'updated_at' => null,
                'weight' => 9,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'Utilization Asset',
                'category_id' => 6,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:44:53.149',
                'updated_at' => null,
                'weight' => 5,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'NO',
            ],
            [
                'item_name' => 'Physical Availability',
                'category_id' => 6,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:45:20.494',
                'updated_at' => null,
                'weight' => 6,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'NO',
            ],
            [
                'item_name' => 'Voluntary Turnover Ratio',
                'category_id' => 7,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:45:47.561',
                'updated_at' => null,
                'weight' => 3,
                'mtd_option' => 'MIN',
                'ytd_option' => 'MIN',
                'is_active' => 'YES',
                'shown_in_focus' => 'YES',
            ],
            [
                'item_name' => 'Training Execution',
                'category_id' => 7,
                'created_by' => null,
                'updated_by' => null,
                'created_at' => '2023-01-24 01:46:03.401',
                'updated_at' => null,
                'weight' => 2,
                'mtd_option' => 'MAX',
                'ytd_option' => 'MAX',
                'is_active' => 'YES',
                'shown_in_focus' => 'NO',
            ],
        ];

        foreach ($items as $item) {
            MasterKPIItemCategory::create(
                $item
            );
        }
    }
}
