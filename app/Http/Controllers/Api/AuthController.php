<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\MasterSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function index()
    {
        return response()
            ->json(['status' => 'Unauthicanted'], 401);
    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'nik' => 'required',
                'password' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        if (auth()->attempt($validator->validated())) {
            $user = $request->user();
            $user['employee'] = $user->employee;
            $user['site'] = MasterSite::find(
                $user->employee->SiteId
            );
            $token = $user->createToken($user->nik)->plainTextToken;
            return response()
                ->json([
                    'status' => 'success',
                    'message' => 'Login successful',
                    'data' => [
                        'user' => $user,
                        'access_token' => $token,
                    ]
                ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Invalid credentials',
            ], 401);
        }
    }

    public function register()
    {
        //
    }

    public function logout(Request $request)
    {
        $request
            ->user()
            ->currentAccessToken()
            ->delete();

        return redirect()->route(route: 'login');
    }
}
