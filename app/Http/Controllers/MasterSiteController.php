<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterSite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(MasterSite::with('cluster')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'companyId' => 'required|max:4',
                'siteID' => 'required|max:3|unique:ms_site,siteID',
                'siteName' => 'required',
                'cluster_id' => 'required|exists:ms_cluster,id'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['createdBy'] = auth()->user()->nik;
        $data['updatedBy'] = auth()->user()->nik;
        $site = MasterSite::create(
            $data
        );
        return (new MasterResource($site))
            ->additional(['message' => 'Site created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterSite  $masterSite
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $site = MasterSite::with('cluster')->find($id);
        if (!$site) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Cluster not found'
                    ],
                    404
                );
        }
        return new MasterResource($site);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterSite  $masterSite
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $site = MasterSite::find($id);
        if (!$site) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Site not found'
                    ],
                    404
                );
        }
        $validator = Validator::make(
            $request->all(),
            [
                'companyId' => 'required|max:4',
                'siteName' => 'required',
                'cluster_id' => 'required|exists:ms_cluster,id'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['updatedBy'] = auth()->user()->nik;
        $site->update($data);
        return (new MasterResource($site))
            ->additional(['message' => 'Cluster updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterSite  $masterSite
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $site = MasterSite::find($id);
        if (!$site) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Site not found'
                    ],
                    404
                );
        }
        $site->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Site deleted'
                ],
                200
            );
    }
}
