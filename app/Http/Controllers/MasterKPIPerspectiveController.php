<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterKPIPerspective;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterKPIPerspectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(
            MasterKPIPerspective::with('categories')
                ->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['perspective_item' => 'required|unique:ms_kpi_perspective,perspective_item']
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['createdBy'] = auth()->user()->nik;
        $data['updatedBy'] = auth()->user()->nik;
        $perspective = MasterKPIPerspective::create(
            $data
        );
        return (new MasterResource($perspective))
            ->additional(['message' => 'Perspective created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKPIPerspective  $masterKPIPerspective
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $perspective = MasterKPIPerspective::with('categories')->find($id);
        if (!$perspective) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Perspective not found'
                    ],
                    404
                );
        }
        return new MasterResource($perspective);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKPIPerspective  $masterKPIPerspective
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $perspective = MasterKPIPerspective::find($id);
        if (!$perspective) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Perspective not found'
                    ],
                    404
                );
        }
        $validator = Validator::make(
            $request->all(),
            ['perspective_item' => 'required|unique:ms_kpi_perspective,perspective_item']
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['updated_by'] = auth()->user()->nik;

        $perspective->update(
            $data
        );
        return (new MasterResource($perspective))
            ->additional(['message' => 'Cluster updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKPIPerspective  $masterKPIPerspective
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $perspective = MasterKPIPerspective::find($id);
        if (!$perspective) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Perspective not found'
                    ],
                    404
                );
        }
        $perspective->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Perspective deleted'
                ],
                200
            );
    }
}
