<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterCompanyCode;
use Illuminate\Http\Request;

class MasterCompanyCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(MasterCompanyCode::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterCompanyCode  $masterCompanyCode
     * @return \Illuminate\Http\Response
     */
    public function show(MasterCompanyCode $masterCompanyCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterCompanyCode  $masterCompanyCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterCompanyCode $masterCompanyCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterCompanyCode  $masterCompanyCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterCompanyCode $masterCompanyCode)
    {
        //
    }
}
