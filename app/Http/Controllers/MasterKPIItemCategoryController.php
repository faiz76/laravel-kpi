<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterKPICategory;
use App\Models\MasterKPIItemCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MasterKPIItemCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(
            MasterKPIItemCategory::with(['perspective', 'category'])
                ->get()
                ->whereNotNull('perspective')
                ->whereNotNull('category')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'item_name' => 'required|max:60',
                'category_id' => 'exists:ms_kpi_category,id',
                'weight' => 'required|min:0|max:100',
                'mtd_option' => Rule::in(['MAX', 'MIN']),
                'ytd_option' => Rule::in(['MAX', 'MIN']),
                'is_active' => Rule::in(['YES', 'NO']),
                'shown_in_focus' => Rule::in(['YES', 'NO'])
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => validator()->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['created_by'] = auth()->user()->nik;
        $data['updated_by'] = auth()->user()->nik;
        $item_category = MasterKPIItemCategory::create(
            $data
        );
        return (new MasterResource($item_category))
            ->additional(['message' => 'Item category created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKPIItemCategory  $masterKPIItemCategory
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $item_category = MasterKPIItemCategory::with(['perspective', 'category'])
            ->find($id);
        if (!$item_category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Perspective not found'
                    ],
                    404
                );
        }
        return new MasterResource($item_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKPIItemCategory  $masterKPIItemCategory
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $item_category = MasterKPIItemCategory::find($id);
        if (!$item_category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Item Category not found'
                    ],
                    404
                );
        }
        $validator = Validator::make(
            $request->all(),
            [
                'item_name' => 'required|max:60',
                'category_id' => 'exists:ms_kpi_category,id',
                'weight' => 'required|min:0|max:100',
                'mtd_option' => Rule::in(['MAX', 'MIN']),
                'ytd_option' => Rule::in(['MAX', 'MIN']),
                'is_active' => Rule::in(['YES', 'NO']),
                'shown_in_focus' => Rule::in(['YES', 'NO'])
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['updated_by'] = auth()->user()->nik;

        $item_category->update(
            $data
        );
        return (new MasterResource($item_category))
            ->additional(['message' => 'Item category updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKPIItemCategory  $masterKPIItemCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $item_category = MasterKPICategory::find($id);
        if (!$item_category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Item category not found'
                    ],
                    404
                );
        }
        $item_category->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Item category deleted'
                ],
                200
            );
    }
}
