<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterKPICategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterKPICategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(
            MasterKPICategory::with('perspective')
                ->get()
                ->whereNotNull('perspective')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'category_item' => 'required|max:60|unique:ms_kpi_category,category_item',
                'perspective_id' => 'required|exists:ms_kpi_perspective,id',
            ]
        );
        if (!$validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => validator()->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['created_by'] = auth()->user()->nik;
        $data['updated_by'] = auth()->user()->nik;

        $category = MasterKPICategory::create(
            $data
        );
        return (new MasterResource($category))
            ->additional(['message' => 'Perspective created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterKPICategory  $masterKPICategory
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $category = MasterKPICategory::with('perspective')->find($id);
        if (!$category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Category not found'
                    ],
                    404
                );
        }
        return new MasterResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterKPICategory  $masterKPICategory
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $category = MasterKPICategory::find($id);
        if (!$category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Category not found'
                    ],
                    404
                );
        }
        $validator = Validator::make(
            $request->all(),
            [
                'category_item' => 'max:60|unique:ms_kpi_category,category_item',
                'perspective_id' => 'exists:ms_kpi_perspective,id',
            ]
        );
        if ($validator->fails()) {
            if ($validator->fails()) {
                return response()
                    ->json(
                        [
                            'status' => 'error',
                            'message' => $validator->errors()
                        ],
                        422
                    );
            }
        }
        $data = $validator->validated();
        $data['updated_by'] = auth()->user()->nik;

        $category->update(
            $data
        );
        return (new MasterResource($category))
            ->additional(['message' => 'Category updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterKPICategory  $masterKPICategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $category = MasterKPICategory::find($id);
        if (!$category) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Category not found'
                    ],
                    404
                );
        }
        $category->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Category deleted'
                ],
                200
            );
    }
}
