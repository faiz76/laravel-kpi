<?php

namespace App\Http\Controllers;

use App\Http\Resources\ErrorResource;
use App\Http\Resources\MasterResource;
use App\Models\MasterCluster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterClusterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(MasterCluster::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'cluster_code' => 'required|unique:ms_cluster,cluster_code|max:3',
                'cluster_description' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $cluster = MasterCluster::create(
            $validator->validated()
        );
        return (new MasterResource($cluster))
            ->additional(['message' => 'Cluster created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterCluster  $masterCluster
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $cluster = MasterCluster::with('sites')->find($id);
        if (!$cluster) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Cluster not found'
                    ],
                    404
                );
        }
        return new MasterResource($cluster);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterCluster  $masterCluster
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $cluster = MasterCluster::find($id);
        if (!$cluster) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Cluster not found'
                    ],
                    404
                );
        }
        $validator = Validator::make(
            $request->all(),
            [
                'cluster_code' => 'required|unique:ms_cluster,cluster_code',
                'cluster_description' => 'required'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $cluster->update($validator->validated());
        return (new MasterResource($cluster))
            ->additional(['message' => 'Cluster updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterCluster  $masterCluster
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $cluster = MasterCluster::find($id);
        if (!$cluster) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Cluster not found'
                    ],
                    404
                );
        }
        $cluster->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Cluster deleted successfully'
                ],
                200
            );
    }
}
