<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterKPIPerspective;
use App\Models\TransaksiKPIHeader;
use Illuminate\Http\Request;

class TransaksiKPIHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(TransaksiKPIHeader::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiKPIHeader  $transaksiKPIHeader
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $tr_header = TransaksiKPIHeader::with('kpi_detail')->find($id);
        if (!$tr_header) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Transaksi header not found'
                    ],
                    404
                );
        }
        foreach ($tr_header->kpi_detail as $detail) {
            $detail->item_category;
            $detail['item_category'] = $detail->item_category->category;
            $detail['perspective'] = $detail->item_category->perspective;
        }
        return new MasterResource($tr_header);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransaksiKPIHeader  $transaksiKPIHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TransaksiKPIHeader $transaksiKPIHeader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiKPIHeader  $transaksiKPIHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiKPIHeader $transaksiKPIHeader)
    {
        //
    }
}
