<?php

namespace App\Http\Controllers;

use App\Http\Resources\MasterResource;
use App\Models\MasterSite;
use App\Models\TransaksiKPIDetail;
use App\Models\TransaksiKPIHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TransaksiKPIDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MasterResource(
            TransaksiKPIDetail::with(['kpi_header', 'item_category'])
                ->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'site_code' => Rule::in(
                    MasterSite::all()
                        ->map(
                            function ($item) {
                                return $item->siteID;
                            }
                        )
                ),
                'data_date' => 'required',
                'item_category' => 'exists:ms_kpi_item_category,id',
                'target' => 'integer',
                'actual' => 'integer'
            ]
        );
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $date = Carbon::parse($data['data_date']);
        $valueDate = $date->startOfMonth()->format('Y-m-d');
        $tr_header = TransaksiKPIHeader::whereMonth('data_date', $date->month)
            ->whereYear('data_date', $date->year)
            ->first();
        if (!$tr_header) {
            $tr_header = TransaksiKPIHeader::create(
                [
                    'site_code' => $data['site_code'],
                    'data_date' => $valueDate
                ]
            );
        }
        $tr_detail = TransaksiKPIDetail::create([
            'header_id' => $tr_header->id,
            'item_id' => $data['item_category'],
            'target' => $data['target'],
            'actual' => $data['actual'],
        ]);
        return (new MasterResource($tr_detail))
            ->additional(['message' => 'Transaksi detail created'])
            ->response()
            ->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiKPIDetail  $transaksiKPIDetail
     * @return \Illuminate\Http\Response
     */
    public function show(string $id)
    {
        $tr_detail = TransaksiKPIDetail::with(['item_category'])
            ->find($id);
        if (!$tr_detail) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Transaksi detail not found'
                    ],
                    404
                );
        }
        return new MasterResource($tr_detail);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TransaksiKPIDetail  $transaksiKPIDetail
     * @return \Illuminate\Http\Response
     */
    public function update(string $id, Request $request)
    {
        $tr_detail = TransaksiKPIDetail::with(['kpi_header', 'item_category'])
            ->find($id);
        if (!$tr_detail) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Transaksi detail not found'
                    ],
                    404
                );
        }
        $validator = Validator::make($request->all(), [
            // 'site_code' => Rule::in(MasterSite::all()->map(function ($item) {
            //     return $item->siteID;
            // })),
            // 'data_date' => 'required',
            'item_category' => 'exists:ms_kpi_item_category,id',
            'target' => 'integer',
            'actual' => 'integer'
        ]);
        if ($validator->fails()) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => $validator->errors()
                    ],
                    422
                );
        }
        $data = $validator->validated();
        $data['updated_by'] = auth()->user()->nik;

        $tr_detail->update(
            $data
        );
        return (new MasterResource($tr_detail))
            ->additional(['message' => 'Transaksi detail updated'])
            ->response()
            ->setStatusCode(200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiKPIDetail  $transaksiKPIDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(string $id)
    {
        $tr_detail = TransaksiKPIDetail::find($id);
        if (!$tr_detail) {
            return response()
                ->json(
                    [
                        'status' => 'error',
                        'message' => 'Transaksi detail not found'
                    ],
                    404
                );
        }
        $tr_detail->delete();
        return response()
            ->json(
                [
                    'status' => 'success',
                    'message' => 'Transaksi detail deleted'
                ],
                200
            );
    }
}
