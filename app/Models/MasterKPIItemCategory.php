<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterKPIItemCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_kpi_item_category';

    protected $fillable = [
        'item_name',
        'category_id',
        'weight',
        'mtd_option',
        'ytd_option',
        'is_active',
        'shown_in_focus',
        'updated_by',
        'created_by'
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(
            MasterKPICategory::class,
            'category_id',
            'id'
        );
    }

    public function perspective(): HasOneThrough
    {
        return $this->hasOneThrough(
            MasterKPIPerspective::class,
            MasterKPICategory::class,
            'id',
            'id',
            'category_id',
            'perspective_id'
        );
    }
}
