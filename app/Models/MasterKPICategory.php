<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterKPICategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_kpi_category';

    protected $fillable = [
        'category_item',
        'perspective_id',
        'updated_by',
        'created_by'
    ];

    public function perspective(): BelongsTo
    {
        return $this->belongsTo(
            MasterKPIPerspective::class,
            'perspective_id',
            'id'
        );
    }

    public function item_categories(): HasMany
    {
        return $this->hasMany(
            MasterKPIItemCategory::class,
            'category_id',
            'id'
        );
    }
}
