<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterCompanyCode extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_company_code';

    public $timestamps = false;

    protected $primaryKey = 'company_code';

    public $incrementing = false;

    protected $keyType = 'string';
}
