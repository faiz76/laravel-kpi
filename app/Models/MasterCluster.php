<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterCluster extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_cluster';

    protected $fillable = [
        'cluster_code',
        'cluster_description',
    ];

    public function sites(): HasMany
    {
        return $this->hasMany(
            MasterSite::class,
            'cluster_id',
            'id'
        );
    }
}
