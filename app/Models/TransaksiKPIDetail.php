<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransaksiKPIDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "tr_kpi_detail";

    protected $fillable = [
        "header_id",
        "item_id",
        "target",
        "actual"
    ];

    // protected $casts = [
    //     'created_at' => 'datetime:d F Y',
    // ];
    
    public function kpi_header(): BelongsTo
    {
        return $this->BelongsTo(
            TransaksiKPIHeader::class,
            'header_id',
            'id'
        );
    }

    public function item_category(): BelongsTo
    {
        return $this->belongsTo(
            MasterKPIItemCategory::class,
            'item_id',
            'id'
        );
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(
            MasterKPICategory::class,
            'category_id',
            'id'
        );
    }
}
