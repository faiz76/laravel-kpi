<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\SoftDeletes;

class TransaksiKPIHeader extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "tr_kpi_header";
    
    protected $fillable = [
        "site_code",
        "data_date"
    ];

    protected $casts = [
        'data_date' => 'datetime:F Y',
        'created_at' => 'datetime:d F Y',
    ];

    public function kpi_detail()
    {
        return $this->hasMany(
            TransaksiKPIDetail::class,
            'header_id',
            'id'
        );
    }

    public function item_category(): HasManyThrough
    {
        return $this->hasManyThrough(
            MasterKPIItemCategory::class,
            TransaksiKPIDetail::class,
            'header_id',
            'id',
            'id',
            'item_id'
        );
    }
}
