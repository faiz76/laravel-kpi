<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    use HasFactory;

    protected $table = 'ms_employee';

    protected $fillable = [
        'EmployeeID',
        'EmployeeName',
        'EmployeeAddress',
        'EmployeePhone',
        'SiteId',
        'DeptID',
        'CreatedBy',
        'UpdatedBy',
        'EMAIL_ADDRESS',
        'EmploymentType',
        'Class',
        'Status',
        'PositionName'
    ];

    protected $primaryKey = 'EmployeeID';

    public function user(): BelongsTo
    {
        return $this->belongsTo(
            User::class,
            'id',
            'EmployeeID'
        );
    }

    public function site()
    {
        
    }
}
