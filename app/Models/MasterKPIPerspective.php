<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterKPIPerspective extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_kpi_perspective';

    protected $fillable = [
        'perspective_item',
        'updated_by',
        'created_by'
    ];

    public function categories(): HasMany
    {
        return $this->hasMany(
            MasterKPICategory::class,
            'perspective_id',
            'id'
        );
    }
}
