<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterSite extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'ms_site';

    protected $fillable = [
        'siteID',
        'companyId',
        'siteName',
        'cluster_id',
        'createdBy',
        'updatedBy',
    ];

    protected $primaryKey = 'siteID';

    public $keyType = 'string';

    public function cluster(): BelongsTo
    {
        return $this->belongsTo(
            MasterCluster::class,
            'cluster_id', 
            'id'
        );
    }
}
