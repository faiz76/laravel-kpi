<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\MasterClusterController;
use App\Http\Controllers\MasterCompanyCodeController;
use App\Http\Controllers\MasterKPICategoryController;
use App\Http\Controllers\MasterKPIItemCategoryController;
use App\Http\Controllers\MasterKPIPerspectiveController;
use App\Http\Controllers\MasterSiteController;
use App\Http\Controllers\TransaksiKPIDetailController;
use App\Http\Controllers\TransaksiKPIHeaderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function() {
    Route::get('/login', 'index')->name('login');
    Route::post('/login', 'login');
    Route::post('/register', 'register');
    Route::get('/logout', 'logout')->middleware('auth:sanctum');
});

Route::middleware('auth:sanctum')->group(function() {
    Route::apiResource('mscluster', MasterClusterController::class);
    Route::apiResource('mssite', MasterSiteController::class);
    Route::apiResource('mskpiperspective', MasterKPIPerspectiveController::class);
    Route::apiResource('mskpicategory', MasterKPICategoryController::class);
    Route::apiResource('mskpiitemcategory', MasterKPIItemCategoryController::class);
    Route::apiResource('mscompanycode', MasterCompanyCodeController::class);
    Route::apiResource('trkpidetail', TransaksiKPIDetailController::class);
    Route::apiResource('trkpiheader', TransaksiKPIHeaderController::class);
});